package main

import (
	"context"
	"log"

	"gitlab.com/juicemia/cloudix/orca/rpc"
	"google.golang.org/grpc"
)

func main() {
	log.Println("booting turtle shell")

	conn, err := grpc.Dial("localhost:9999", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("error dialing grpc: %v", err)
	}

	client := rpc.NewOrcaClient(conn)

	resp, err := client.Exec(context.Background(), &rpc.ExecRequest{Command: "foo"})
	if err != nil {
		log.Fatalf("error calling Test: %v", err)
	}

	log.Printf("resp code: %d", resp.Code)
}
