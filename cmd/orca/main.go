package main

import (
	"log"

	"gitlab.com/juicemia/cloudix/orca/rpc"
)

func main() {
	log.Println("booting orca server")

	log.Fatalf("%v", rpc.ListenAndServe())
}
