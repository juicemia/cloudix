package rpc

import (
	context "context"
	"log"
	"net"

	grpc "google.golang.org/grpc"
)

func ListenAndServe() error {
	lis, err := net.Listen("tcp", ":9999")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	srv := grpc.NewServer()
	RegisterOrcaServer(srv, &orcaServer{})

	return srv.Serve(lis)
}

type orcaServer struct{}

func (*orcaServer) Exec(ctx context.Context, req *ExecRequest) (*ExecResponse, error) {
	log.Printf("got request %+v", req)

	return &ExecResponse{
		Code: int32(0),
	}, nil
}
